//
//  Room.m
//  OOP Demo
//
//  Created by James Cash on 10-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Room.h"

@implementation Room

- (instancetype)initWithWidth:(float)width depth:(float)depth height:(float)height windowArea:(float)windowArea
{
    self = [super init];
    if (self) {
        _width = width;
        _depth = depth;
        _height = height;
        _windowArea = windowArea;
    }
    return self;
}

- (float)volume
{
    return self.width * self.height * self.depth;
}

- (float)brightness
{
    return self.windowArea / [self volume];
}

- (BOOL)isBrighterThan:(Room*)otherRoom
{
    return [self brightness] > [otherRoom brightness];
}

@end
