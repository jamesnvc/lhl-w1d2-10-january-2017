//
//  Room.h
//  OOP Demo
//
//  Created by James Cash on 10-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Room : NSObject

@property float width;
@property float height;
@property float depth;
@property float windowArea;

/* made by the @property
 { float _width; }
- (float)width;
- (void)setWidth:(float)newWidth;
*/

- (instancetype)initWithWidth:(float)width depth:(float)depth height:(float)height windowArea:(float)windowArea;

- (float)volume;
-(float)brightness;

- (BOOL)isBrighterThan:(Room*)otherRoom;

@end
