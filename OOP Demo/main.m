//
//  main.m
//  OOP Demo
//
//  Created by James Cash on 10-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Room.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        /*
        float room1Width = 100.0;
        float room1Depth = 50.0;
        float room1Height = 10.0;
        float room1Volume = room1Width * room1Height * room1Depth;

        float room1WindowArea = 25.0f;
        float room1Brightness = room1WindowArea / room1Volume;

        NSLog(@"Room1 brightness = %f", room1Brightness);
         */
        Room *room1 = [[Room alloc] initWithWidth:100.0 depth:50.0 height:10.0 windowArea:100.0];
        /*
        room1.width = 100.0;
        room1.height = 10.0;
        room1.depth = 50.0;
        room1.windowArea = 100.0;
         */

        Room *room2 = [[Room alloc] init];
        room2.width = 50.0;
        room2.height = 5.0;
        room2.depth = 20.0;
        room2.windowArea = 50.0;
              
        NSLog(@"Room 1 volume = %f, room 2 volume = %f", [room1 volume],
              [room2 volume]);
        NSLog(@"Room 1 brightness = %f, room 2 brightness = %f", [room1 brightness],
              [room2 brightness]);

        if ([room1 isBrighterThan:room2]) {
            NSLog(@"Room 1 is brighter");
        } else {
            NSLog(@"Room 2 is brighter (or the same)");
        }

        Room *room3 = [[Room alloc] initWithWidth:25 depth:25 height:25 windowArea:100];

        if ([room2 isBrighterThan:room3]) {
            NSLog(@"Room 2 is brighter");
        } else {
            NSLog(@"Room 3 is brighter (or the same)");
        }

        NSArray<Room*>* rooms = @[room1, room2, room3];
        NSLog(@"We have %lu room", (unsigned long)[rooms count]);
        for (Room *room in rooms) {
            NSLog(@"Room has volume %f", [room volume]);
        }
        NSLog(@"Second room has brightness %f", [rooms[1] brightness]);
    }
    return 0;
}
